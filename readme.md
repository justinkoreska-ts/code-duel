# Code Duel

Code Duel is a coding game!

Players take turns adding code to build an app (preferably something fun like a game).

## Goal

The goal is simply to have fun with a friend and maybe learn new tricks.

## Rules

0. Each player takes a turn by adding a commit to the app repo.
0. Each commit must introduce new behavior to the app.
0. The app must compile and run (run-time errors and crashes are acceptable).
0. Each commit can add up to 10 lines, modify up to 50% of existing lines, and delete lines.
0. Whitespace- and comment-only lines are not counted.
0. Players may not discuss or coordinate changes.

## Refactor Round

TBD
